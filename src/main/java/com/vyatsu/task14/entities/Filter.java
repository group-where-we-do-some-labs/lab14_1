package com.vyatsu.task14.entities;

public class Filter {

    public static String substr = "";

    public static PriceType type = PriceType.NONE;
    private  int max = Integer.MAX_VALUE;
    private  int min = -1;

    public PriceType getType() {
        return type;
    }

    public  void setType(PriceType type) {
        Filter.type = type;
    }

    public String getSubstr() {
        return substr;
    }

    public  void setSubstr(String substr) {
        Filter.substr = substr;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }
}
