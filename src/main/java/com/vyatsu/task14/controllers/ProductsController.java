package com.vyatsu.task14.controllers;

import com.sun.deploy.appcontext.AppContext;
import com.vyatsu.task14.entities.Filter;
import com.vyatsu.task14.entities.PriceType;
import com.vyatsu.task14.entities.Product;
import com.vyatsu.task14.services.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.Console;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/products")
public class ProductsController {
    private ProductsService productsService;
    private final Set<Long> editing = new HashSet<>();
    private final Filter filter = new Filter();
    private Pattern pattern = Pattern.compile("");


    @Autowired
    public void setProductsService(ProductsService productsService) {
        this.productsService = productsService;
    }

    @GetMapping
    public String showProductsList(Model model) {
        Product product = new Product();
        Product product1 = new Product();
        List<Product> productList = new ArrayList<>();
        for (Product p: productsService.getAllProducts()) {
            if(pattern.matcher(p.getTitle()).find()){
                productList.add(p);
            }
        }
        productList = productList.stream().filter(p -> p.getPrice() >= filter.getMin() && p.getPrice() <= filter.getMax()).collect(Collectors.toList());
        if(filter.getType().equals(PriceType.DESCENDING)){
            productList = productList.stream().sorted(Comparator.comparingInt(Product::getPrice)).collect(Collectors.toList());
        }
        if(filter.getType().equals(PriceType.ASCENDING)){
            productList = productList.stream().sorted((p1, p2) -> p2.getPrice() - p1.getPrice()).collect(Collectors.toList());
        }



        model.addAttribute("products", productList);
        model.addAttribute("product", product);
        model.addAttribute("editing", editing);
        model.addAttribute("product1", product1);
        model.addAttribute("filter", filter);
        return "products";
    }

    @PostMapping("/add")
    public String addProduct(@ModelAttribute(value = "product") Product product) {
        productsService.add(product);
        return "redirect:/products";
    }

    @GetMapping("/show/{id}")
    public String showOneProduct(Model model,
                                 @PathVariable(value = "id") Long id) {
        Product product = productsService.getById(id);
        model.addAttribute("product", product);
        return "product-page";
    }

    @GetMapping("/delete/{id}")
    public String deleteOneProduct(Model model,
                                   @PathVariable(value = "id") Long id) {
        productsService.getAllProducts().stream()
                .filter(p -> Objects.equals(p.getId(), id))
                .findFirst()
                .ifPresent(product -> productsService.delete(product));
        return "redirect:/products";
    }

    @GetMapping("/edit/{id}")
    public String editOneProduct(@PathVariable(value = "id") Long id) {
        editing.add(id);
        System.out.println(editing);

        return "redirect:/products";
    }

    @PostMapping("/edit")
    public  String editProduct(@ModelAttribute(value = "product1") Product product){
        Product edited = productsService.getAllProducts()
                .stream()
                .filter(p -> p.getId() == product.getId())
                .findFirst()
                .orElse(null);
        edited.setTitle(product.getTitle());
        edited.setPrice(product.getPrice());
        editing.remove(product.getId());
        return "redirect:/products";
    }


    @PostMapping ("/filter")
    public String filterList( @ModelAttribute(value = "filter") Filter f){
        pattern = Pattern.compile(f.getSubstr());
        filter.setSubstr(f.getSubstr());
        filter.setType(f.getType());
        filter.setMax(f.getMax());
        filter.setMin(f.getMin());

        return "redirect:/products";
    }

    @PostMapping("/clearFilter")
    public String clearFilter() {
        pattern = Pattern.compile("");
        filter.setSubstr("");
        filter.setType(PriceType.NONE);
        filter.setMax(Integer.MAX_VALUE);
        filter.setMin(-1);

        return "redirect:/products";
    }

}
